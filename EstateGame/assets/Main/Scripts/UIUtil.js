
module.exports = {

    InitGameBundle(_Url,_Version = null){
        const options = {
            version:_Version,
            onFileProgress:(n,t)=>{
                console.log("==========进度===========",n / t)
            },
        }
        cc.assetManager.loadBundle(_Url,options,(err,bundle)=>{
            bundle.load("Login/Login",cc.Prefab,(err,prefab) => {
                if(err){
                    console.log("加载Login失败",err)
                    return
                }
                console.log("加载Login成功",prefab)
                var tempNode = cc.instantiate(prefab);
                var scene = cc.director.getScene()
                scene.insertChild(tempNode , 10);
            });
        });
    },

    LoadPrefab(_Name){
        var Bundle = cc.assetManager.getBundle("GamesBundle")
        if(!Bundle){
            Bundle.load(_Name + "/" + _Name,cc.Prefab,(err,prefab) => {
                if(err){
                    console.log("加载prefab失败",err)
                    return
                }
                console.log("加载prefab成功",prefab)
                var tempNode = cc.instantiate(prefab);
                var scene = cc.director.getScene()
                scene.insertChild(tempNode , 10);
            });
        }else{
            console.log("获取GamesBundle失败")
            return
        }
    }
};
