var HttpMgr = require('HttpMgr');
var AppConfig = require("AppConfig")
var UIUtil = require("UIUtil")
module.exports = {
    versionCode : 1,
    versionName : "1.0.1",

    newVersionCode : 1,
    newVersionName : "1.0.1",

    updateInfo : null,
    newUpdateInfo : null,

    init(){
        const updateInfo = JSON.parse(cc.sys.localStorage.getItem("updateInfo"))
        if (updateInfo) {
            const infoObj = updateInfo;
            this.versionCode = infoObj.versionCode;
            this.versionName = infoObj.versionName;
        } else {
            this.versionCode = AppConfig.VERSION_CODE;
            this.versionName = AppConfig.VERSION;
        }
        this.updateInfo = updateInfo
        console.log("当前游戏版本:", this.versionCode, this.versionName);
    },

    doUpdate() {
        if (this.updateInfo && this.hasNewVersion()) {
            cc.sys.localStorage.setItem("updateInfo", JSON.stringify(this.newUpdateInfo));
            return true
        }
        return false
    },

    CheckUpdate(){
        if (!AppConfig.UpdateFileUrl) {
            cc.warn("未配置更新检测地址");
            return;
        }
        HttpMgr.get(AppConfig.UpdateFileUrl,(responseText) => {
            console.log("===========responseText======",responseText)
            this.newUpdateInfo = JSON.parse(responseText)
            if (this.newUpdateInfo) {
                console.log("请求到新版本资源:", this.newUpdateInfo);
                this.newVersionName = this.newUpdateInfo.versionName;
                this.newVersionCode = this.newUpdateInfo.versionCode;
                if(this.doUpdate()){
                    console.log("=======有更新===========版本号：",this.newUpdateInfo.versionCode)
                    this.loadNewBundle(this.newUpdateInfo)
                }else{
                    console.log("=======没有更新===========",this.updateInfo)
                    console.log("========查询缓存======",cc.assetManager.cacheManager.cacheDir)
                    console.log("========查询缓存2======",cc.assetManager.cacheManager.getCache())
                    this.loadNewBundle(this.updateInfo)
                }
            }
        })
    },

    loadNewBundle(_UpdateInfo){
        UIUtil.InitGameBundle(_UpdateInfo.server,_UpdateInfo.bundles["GamesBundle"])
    },

    hasNewVersion() {
        return this.newVersionCode != this.versionCode;
    }
};