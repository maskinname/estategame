
module.exports = {
    /**
     * 加载资源
     * @param {*} _path 加载资源地址 
     * @param {*} _callBack 加载资源成功回调
     */
    GameBundleFloder : "GamesBundle",

    BuildType : cc.Enum({
        Debug : -1,
        Release : -1,
    })
};
