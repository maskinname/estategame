/**
 * @description 数据监听函数
 */
"use strict";
function DataNotify(){
    this.__listeners = {};
    this.__properties = {};
    this.__defaultValues = {};
    this.__created = false;

    /**
     * @description 取值
     */
    this._getValue = function(_propName){
        if(this.__listeners.hasOwnProperty(_propName)){
            return this.__listeners[_propName].after;
        }

        return this.__properties[_propName];
    };

    /**
     * @description 更新值，并触发回调
     */
    this._update = function(_propName, _val){
        this.__properties[_propName] = _val;
        if(!this.__listeners.hasOwnProperty(_propName)) return;
        var listener = this.__listeners[_propName];
        let before = listener.before;
        listener.after = listener.before = _val;
        listener.configs.forEach((lis)=>{
            if(_val !== null && _val !== undefined)
            {
                lis.callback.call(lis.target, _val, before)
            }
        });
    }


    /**
     * @description 绑定数组相关函数，调用数据相关函数时也会触发回调
     */
    this._tryBindArrayFunction = function(_propName) {
        if (this[_propName] instanceof Array) {
            var arrProto = Object.create(Array.prototype);
            var self = this;
            ['shift','unshift','push','pop','splice'].forEach((method) => {
                Object.defineProperty(arrProto, method,{
                    value: function(){
                        var result = Array.prototype[method].apply(this, arguments);
                        self._update(_propName, self[_propName])
                        return result;
                    }
                })
            })
            this[_propName].__proto__ = arrProto
        } 
    }

    /**
     * @description 向目标属性添加监听函数
     * @param {*} propertyName 
     * @param {*} callback 
     * @param {*} target 
     */
    this.AddListener = function(propertyName, callback, target){
        if(this.__listeners.hasOwnProperty(propertyName)){
            this.__listeners[propertyName].configs.push({
                callback : callback,
                target : target
            });
        }else{
            this.__listeners[propertyName] = {
                configs : [{
                    callback : callback,
                    target : target
                }],
                before : this[propertyName],
                after : this[propertyName]
            }
        }

        if(this[propertyName] != null && this[propertyName] != undefined && callback && target)
        {
            callback.call(target, this[propertyName], null);
        }
    };

    /**
     * @description 删除某个作用域下的所有监听函数
     * @param {*} target 
     */
    this.RemoveListenerByTarget = function(target){
        if(!target){
            cc.LogUtil.Log("缺少必要参数target",cc.LogUtil.EnvOption.outertest);
            return
        }
    
        for(var key in this.__listeners)
        {
            var Step = 0;
            while(this.__listeners[key].configs.length >0 && Step < this.__listeners[key].configs.length)
            {
                if(this.__listeners[key].configs[Step].target == target)
                {
                    this.__listeners[key].configs.splice(Step, 1);
                    Step = 0;
                }
                else
                {
                    Step++;
                }
            }
        }
    };

    /**
     * @description 删除某个监听函数
     * @param {*} target 
     */
    this.RemoveListenerByProp = function(_prop, _target){
        if(!_target || !_prop){
            cc.LogUtil.Log("缺少必要参数",cc.LogUtil.EnvOption.outertest);
            return
        }

        for(var i=0; i<this.__listeners[_prop].configs.length; i++){
            if(this.__listeners[_prop].configs[i].target == _target){
                this.__listeners[_prop].configs.splice(i, 1);
            }
        }
    };

    /**
     * @description 清除所有属性的监听
     */
    this.ClearListeners = function(){
        this.__listeners = null;
        this.__listeners = {};
    };

    /**
     * @description 创建属性监听
     */
    this.CreateNotify = function(){
        if(this.__created) return;

        var props = Object.keys(this);
        var propertyDefine = {};
        props.forEach((prop)=>{
            this.__properties[prop] = this[prop];
            this.__defaultValues[prop] = this[prop];
            this._tryBindArrayFunction(prop);
            propertyDefine[prop] = {
                get(){ return this._getValue(prop) },
                set(val){ 
                    this._update(prop, val);
                }
            }
        });
        Object.defineProperties(this, propertyDefine);

        this.__created = true;
    };

    this.toJSONObject = function(){
        var props = Object.keys(this);
        var result = {};
        props.forEach((prop) => {
            result[prop] = this[prop];
        });
        return result;
    }

    /**
     * @description 重置数据
     */
    this.reset = function()
    {
        this.resetDataToDefault();
        this.ClearListeners();
    }

    this.resetToNull = function(){
        var props = Object.keys(this);
        props.forEach((prop) => {
            var DefaultValue  =this.__defaultValues[prop];
            this[prop]  = DefaultValue;
        });
    }

    this.resetDataToDefault = function()
    {
        var props = Object.keys(this);
        props.forEach((prop) => {
            var DefaultValue  =this.__defaultValues[prop];
            if(typeof DefaultValue == "function")
            {

            }
            else
            {
                if("__listeners" == prop)
                {

                }
                else
                {
                    this[prop]  = DefaultValue;
                }
            }

        });
    }
}
module.exports = DataNotify;
