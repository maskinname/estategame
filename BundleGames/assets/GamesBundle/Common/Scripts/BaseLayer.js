

var BaseLayer =  cc.Class({
    extends: cc.Component,

    properties: {
        m_Prefabs : {
            default : [],
            type : cc.Prefab
        }
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.PreCustomerInit();
        this.CustomerInit();
    },

    onDestroy()
    {
        this.CustomerRelease();
    },

    //子类覆盖这个方法，处理自定义的一些初始化事件
    PreCustomerInit()
    {

    },


    //子类覆盖这个方法，处理自定义的一些初始化事件
    CustomerInit()
    {

    },

    //子类覆盖这个方法，处理自定义的一些销毁事件
    CustomerRelease()
    {

    },

    // update (dt) {},
});
