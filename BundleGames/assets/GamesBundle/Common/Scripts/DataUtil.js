var LandDataNotify = require("LandDataNotify")
module.exports =
 {
    getLandHightLightInfo(){
        var num = 0;
        var indexArray = [];
        LandDataNotify.HighLightArray.forEach((ele,index) => {
            if(ele){
                num++
                indexArray.push(index)
            }
        });

        return {
            num : num,
            index : indexArray 
        }
    }
};
