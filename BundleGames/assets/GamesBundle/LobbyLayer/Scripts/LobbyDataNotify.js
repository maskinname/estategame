var DataNotify = require('DataNotify');

function LobbyData(){
   
    //移动时间开关
    this.EventIsMove = null,

    //当前选中目标
    this.CurrentTarget = null,

    this.DrawingsData = null,
    // [{
    //     name : "茅草屋",
    //     isLock  : false,
    //     DrawingNumber : 10,
    // },{
    //     name : "小木屋",
    //     isLock  : false,
    //     DrawingNumber : 10,
    // },{
    //     name : "平房",
    //     isLock  : true,
    //     DrawingNumber : 10,
    // },{
    //     name : "公寓",
    //     isLock  : true,
    //     DrawingNumber : 10,
    // },{
    //     name : "别墅",
    //     isLock  : true,
    //     DrawingNumber : 10,
    // },{
    //     name : "大厦",
    //     isLock  : true,
    //     DrawingNumber : 10,
    // }]


    this.ResetCommonWindow = function()
    {

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////

	this.CreateNotify();
};

LobbyData.prototype = new DataNotify();
module.exports = new LobbyData();