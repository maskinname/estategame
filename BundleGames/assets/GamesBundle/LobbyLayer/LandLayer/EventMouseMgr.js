var LobbyDataNotify = require('LobbyDataNotify');
var DataUtil = require('DataUtil');
var BaseLayer = require('BaseLayer');
cc.Class({
    extends: BaseLayer,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    PreCustomerInit () {
        LobbyDataNotify.AddListener("EventIsMove" , this.onEventIsMove , this);
        LobbyDataNotify.AddListener("CurrentTarget" , this.onCurrentTarget , this);
        this.node.on(cc.Node.EventType.MOUSE_MOVE,this.onMouseMove,this);
        this.node.on(cc.Node.EventType.TOUCH_END,this.onTouchEnd,this);
        this.node._touchListener.setSwallowTouches(false);
        this.IsMove = false
    },  

    CustomerRelease()
    {
        LobbyDataNotify.RemoveListenerByTarget(this);
    },

    /**
     * 是否为移动状态
     * @param {*} _data 
     */
    onEventIsMove(_data){
        this.IsMove = !!_data;
        if(_data){
            this.setTargetPositon(this.CurrentTarget,_data.convertToWorldSpaceAR(cc.Vec2(0,0)))
            if(!this.CurrentTarget) return
            this.CurrentTarget.active = true;
            this.node._touchListener.setSwallowTouches(true);
        }
    },

    /**
     * 创建当前节点
     * @param {*} _data 
     */
    onCurrentTarget(_data){
        var Prefab = this.m_Prefabs[_data]
        if(!Prefab){
            console.log("该图集不存在")
            this.CurrentTarget = null
            return
        }
        var CurrentNode = this.node.getChildByName(this.m_Prefabs[_data].name)
        if(!CurrentNode){
            console.log("获取节点不存在",this.m_Prefabs[_data].name)
            this.CurrentTarget = cc.instantiate(this.m_Prefabs[_data])
            if(!this.CurrentTarget){
                console.log("克隆节点失不存在")
                return
            }
            this.CurrentTarget.parent = this.node
            this.CurrentTarget.active = true
        }else{
            console.log("获取节点已存在",this.m_Prefabs[_data].name)
            this.CurrentTarget = CurrentNode
            this.CurrentTarget.active = true
        }
    },

    onMouseMove(_event){
        if(!this.IsMove) return;
        var pos = new cc.Vec2(_event.getLocationX(),_event.getLocationY())
        // console.log("鼠标移动,坐标：",pos)
        this.setTargetPositon(this.CurrentTarget,pos)

    },

    onTouchEnd(_event){
        this.node._touchListener.setSwallowTouches(false);
        if(!this.IsMove) return
        var HLInfo = DataUtil.getLandHightLightInfo()
        console.log("TouchEnd",HLInfo);
        this.IsMove = false
        if(!this.CurrentTarget) {
            console.log("点击抬起,当前图集不存在")
            return
        }
        this.CurrentTarget.active = false;
    },

    setTargetPositon(_Target,pos){
        if(!_Target){
            console.log("目标节点不存在")
            return
        }
        _Target.position = pos
    },

    // update (dt) {
    // },
});
