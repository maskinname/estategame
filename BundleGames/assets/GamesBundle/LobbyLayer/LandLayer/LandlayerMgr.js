var BaseLayer = require('BaseLayer');
var LandDataNotify = require("LandDataNotify")
cc.Class({
    extends: BaseLayer,

    properties: {

    },

    PreCustomerInit()
    {
        LandDataNotify.AddListener("Lands" , this.onLands , this);
    },

    CustomerInit()
    {
        LandDataNotify.Lands = [{
            name : "1号农田",
            isLock : false,
        },
        {
            name : "2号农田",
            isLock : false,
        },{
            name : "3号农田",
            isLock : false,
        },{
            name : "4号农田",
            isLock : false,
        },{
            name : "5号农田",
            isLock : false,
        },{
            name : "6号农田",
            isLock : false,
        },{
            name : "7号农田",
            isLock : false,
        },{
            name : "8号农田",
            isLock : false,
        }]
    },

    CustomerRelease()
    {
        LandDataNotify.RemoveListenerByTarget(this);
    },

    onLands(_data){
        var LandNode = this.node.getChildByName("Land")
        var landChildren = LandNode.children
        console.log("=Lands==",landChildren)
        LandNode.removeAllChildren();
        for (let index = 0; index < _data.length; index++) {
            var element = _data[index];
            var TempNode = cc.instantiate(this.m_Prefabs[0]);
            TempNode.parent = LandNode
            element.Number = index;
            TempNode.getComponent("ColliderListener").initData(element)
        }

        LandDataNotify.HighLightArray = [];
        _data.forEach(element => {
            LandDataNotify.HighLightArray.push(false);
        });
    }

    // update (dt) {},
});
