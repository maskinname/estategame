var DataNotify = require('DataNotify');

function LandData(){
   
    this.Lands = null;
    this.CurrentBuildCount = 3;
    this.HighLightArray = [];

    ////////////////////////////////////////////////////////////////////////////////////////////////////
	this.CreateNotify();
};

LandData.prototype = new DataNotify();
module.exports = new LandData();