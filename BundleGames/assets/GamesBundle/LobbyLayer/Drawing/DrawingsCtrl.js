var BaseLayer = require('BaseLayer');
var LobbyDataNotify = require("LobbyDataNotify")

cc.Class({
    extends: BaseLayer,

    properties: {

    },

    PreCustomerInit()
    {
        LobbyDataNotify.AddListener("DrawingsData" , this.onDrawingsData , this);
    },

    CustomerInit()
    {
        LobbyDataNotify.DrawingsData = 
            [{
        name : "茅草屋",
        isLock  : false,
        DrawingNumber : 10,
    },{
        name : "小木屋",
        isLock  : false,
        DrawingNumber : 10,
    },{
        name : "平房",
        isLock  : true,
        DrawingNumber : 10,
    },{
        name : "公寓",
        isLock  : true,
        DrawingNumber : 10,
    },{
        name : "别墅",
        isLock  : true,
        DrawingNumber : 10,
    },{
        name : "大厦",
        isLock  : true,
        DrawingNumber : 10,
    }]
    },

    CustomerRelease()
    {
        LobbyDataNotify.RemoveListenerByTarget(this);
    },

    onDrawingsData(_data){
        for (let index = 0; index < _data.length; index++) {
            const data = _data[index]
            var TempNode = this.createDrawing()
            TempNode.parent = this.node;
            TempNode.getComponent("DrawingCtrl").initDrawing(data);
            TempNode._name = index;
        }
    },

    createDrawing(){
        var Prefab = this.m_Prefabs[0];
        return cc.instantiate(Prefab);
    },

    // update (dt) {},
});
