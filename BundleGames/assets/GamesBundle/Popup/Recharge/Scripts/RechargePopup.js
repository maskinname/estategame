var BasePopUp = require('BasePopUp');

cc.Class({
    extends: BasePopUp,

    properties: {
    },

    CustomerInit()
    {
    },

    onEnable(){
        this.initChooseToggle()
    },

    initChooseToggle(_Data){
        var ChooseTCL = cc.find("SendItem/ChooseTCLayout",this.node);
        ChooseTCL.active = false;
        ChooseTCL.removeAllChildren();
        for (let index = 0; index < 5; index++) {
            var TempNode = cc.instantiate(this.m_Prefabs[0]) 
            TempNode._name = index;
            TempNode.parent = ChooseTCL;
            TempNode.on("toggle",this.OnChooseToggleContainerClick,this)
        }
    },

    OnSendChooseToggleClick(_Data){
        console.log("====OnSendChooseToggleClick===",_Data)
        var ChooseTCL = cc.find("SendItem/ChooseTCLayout",this.node);
        ChooseTCL.active = _Data.isChecked
    },

    /**
     * 单选框回调
     * @param {*} _Data 
     */
    OnChooseToggleContainerClick(_Data){
        console.log("====OnChooseTCLClick===",_Data.name)
    },

    /**
     * 交换按钮
     */
     onExchangeClick(_Data){
        console.log("====onExchangeClick===",_Data.name)
    },

    


    // update (dt) {},
});
