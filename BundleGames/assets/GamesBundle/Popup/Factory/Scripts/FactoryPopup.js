var BasePopUp = require('BasePopUp');

cc.Class({
    extends: BasePopUp,

    properties: {
        _BuildType : {
            default:null,
            type : cc.Enum({
                EstateMap : -1,
                MyBuild : -1,
            })
        }

    },

    CustomerInit () {

    },

    onEnable(){
        this.BuildTypeData = [{
            name : "房产图NFT",
        },{
            name : "我的房产"
        }]

        this.BuildTypeToggle = cc.find("MainNode/BuildTypeToggle",this.node)
        this.initBuildTypeToggle()
    },

    /**
     * 初始化建筑类型复选框
     */
    initBuildTypeToggle(){
        this.BuildTypeToggle.removeAllChildren()
        for (let index = 0; index < this.BuildTypeData.length; index++) {
            const data = this.BuildTypeData[index];
            var TempNode = cc.instantiate(this.m_Prefabs[0])
            TempNode.parent = this.BuildTypeToggle
            TempNode._name = index
            TempNode.getChildByName("TypeName").getComponent(cc.Label).string = data.name
            TempNode.on("toggle",this.onBuildTypeToggle,this)   
        }
        this.BuildTypeToggle.children[0].emit("toggle",{name : 1});
    },

    /**
     * 复选框回调
     */
    onBuildTypeToggle(_data){
        console.log("onBuildTypeToggle:",_data.name)
    },

    initBuildIconLayout(){
        
    }
    // update (dt) {},
});
