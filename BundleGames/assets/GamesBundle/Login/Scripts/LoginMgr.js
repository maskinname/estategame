var BaseLayer = require('BaseLayer');
var LoginDataNotify = require('LoginDataNotify');
cc.Class({
    extends: BaseLayer,

    properties: {
    },

    PreCustomerInit()
    {
        LoginDataNotify.AddListener("LayersMgr" , this.onLayersMgr , this);
    },

    CustomerInit()
    {
        
    },

    CustomerRelease()
    {
        LoginDataNotify.RemoveListenerByTarget(this);
    },

    onLayersMgr(_data){
        console.log("==========LoginDataNotify===========",_data)
    },

    onTestClick(){
        console.log("===执====行按钮====")
        LoginDataNotify.LayersMgr = "xiangni"
    }
    // update (dt) {},
});
