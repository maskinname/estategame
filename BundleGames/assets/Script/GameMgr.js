cc.Class({
    extends: cc.Component,

    properties: {
        label: {
            default: null,
            type: cc.Label
        },
    },

    // use this for initialization
    onLoad: function () {
        cc.assetManager.loadBundle("GamesBundle", function (err,bundle) {
            if (!err){
                bundle.load("Login/Login",cc.Prefab,(err,prefab) => {
                    if(err){
                        console.log("加载Login失败",err)
                        return
                    }
                    console.log("加载Login成功",prefab)
                    var tempNode = cc.instantiate(prefab);
                    var scene = cc.director.getScene()
                    scene.insertChild(tempNode , 10);
                });
            }
        });
    },

    // called every frame
    update: function (dt) {

    },
});
